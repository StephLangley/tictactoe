from django.db import models


class User(models.Model):
    id = models.CharField(max_length=128, unique=True, primary_key=True)
    win = models.IntegerField(default=0)
    lose = models.IntegerField(default=0)
    draw = models.IntegerField(default=0)
    username = models.CharField(max_length=128)


class Games(models.Model):
    id = models.CharField(max_length=128, unique=True, primary_key=True)
    player1 = models.ForeignKey(
        User, related_name="player1", on_delete=models.CASCADE)
    player2 = models.ForeignKey(
        User, related_name="player2", on_delete=models.CASCADE, null=True)
    board = models.CharField(max_length=128)
    turn = models.CharField(max_length=1)
    created = models.DateTimeField()
    updated = models.DateTimeField()
    winner = models.ForeignKey(
        User, related_name="winner", on_delete=models.CASCADE, null=True)
