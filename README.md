A simple web based Tic Tac Toe game using, React front-end and python to Django 
back-end with a SQLite database to store game, and player data.

- Play player vs player by sharing your URL.
- Access previous games, or continue a game using the games menu.
- Update your user name and view your player profile with the profile menu.

Project Setup
- install python 3 https://www.python.org/downloads/
- install node JS https://nodejs.org/en/download/

To Run Project
- clone repository
- open two terminals to run the frontend and backend

    frontend
    - `cd frontend`
    - `yarn start`
    
    backend
    - `pip install Django`
    - `cd backend\tttdjango`
    - `python manage.py makemigrations`
    - `python manage.py migrate`
    - `python manage.py runserver`