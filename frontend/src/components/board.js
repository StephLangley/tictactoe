import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import useInterval from '../hooks/useInterval';
import './board.css';
import axios from 'axios';

const Board = () => {
    let { id } = useParams();
    const [board, setBoard] = useState([]);
    const [player, setPlayer] = useState('X');
    const [win, setWin] = useState([]);
    const [valid, setValid] = useState('');
    const [username, setusername] = useState('');
    const [counter, setcounter] = useState('X');

    const updatestate = id => {
        axios.get(`/api/state/${id}`, { win: '' }).then(response => {
            setcounter(response.data.counter);
            setusername(response.data.username);
            setBoard(response.data.board.split(','));
            setPlayer(response.data.player);
            setWin(response.data.win);
            setValid(response.data.valid);
        });
    };

    useInterval(() => {
        updatestate(id);
    }, 5000);

    useEffect(() => {
        updatestate(id);
        axios.post(`/api/addplayer/`, { gameid: id });
    }, [id]);

    const handleClick = index => {
        axios.post('/api/turn/', { index, gameid: id }).then(response => {
            setBoard(response.data.board.split(','));
            setPlayer(response.data.player);
            setWin(response.data.win);
            setValid(response.data.valid);
        });
    };

    let header = null;
    let validMove = null;

    if (valid !== '') {
        validMove = (
            <div>
                <h1 className="t-item ">{valid}</h1>
            </div>
        );
    }

    let classwin = '';
    if (win[1] && counter !== player) {
        classwin = 'combo-red';
    } else if (win[1]) {
        classwin = 'combo';
    }

    const grid = board.map((item, index) => {
        return (
            <div className={`piece ${win.includes(index) ? classwin : ''}`} key={index} onClick={e => handleClick(index)}>
                {item}
            </div>
        );
    });

    if (win === '' || win[0] === '') {
        header = <h1 className="t-item">Turn: {player}</h1>;
    } else if (win.length > 1) {
        header = (
            <div>
                <h1 className="t-item">Player: {player} wins!</h1>{' '}
            </div>
        );
    } else if (win[0] === 'draw') {
        header = (
            <div>
                <h1 className="t-item">A Draw!</h1>{' '}
            </div>
        );
    }

    return (
        <div>
            <h1 className="t-item">Username: {username}</h1>
            <h1 className="t-item">Counter: {counter}</h1>
            <div>
                <div className="tag">
                    {header}
                    {validMove}
                </div>
                <div className="board">{grid}</div>
            </div>
            <copy-button className="copy">{window.location.href}</copy-button>
        </div>
    );
};

export default Board;
