import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import './games.css';
import axios from 'axios';

const Games = () => {
    const [games, setGames] = useState([]);
    const [user, setUser] = useState('');

    useEffect(() => {
        axios.get('/api/games').then(response => {
            setGames(response.data.games);
            setUser(response.data.user);
        });
    }, []);

    const grid = games.map((item, index) => {
        let classwin = '';

        if (item.winner === user) {
            classwin = 'combo';
        } else if (item.winner) {
            classwin = 'combo-red';
        } else if (item.completed) {
            classwin = 'combo-draw';
        }

        return (
            <Link className={`item ${classwin}`} key={index} to={`/game/${item.id}`}>
                <div>
                    <ul>
                        <li>Game ID: {item.id}</li>
                        <li>Player 1: {item.player1}</li>
                        <li>Player 2: {item.player2}</li>
                        <li>Last Updated: {item.lastupdated}</li>
                        <li>Winner: {item.winner}</li>
                    </ul>
                </div>
            </Link>
        );
    });
    return <div className={'grid'}>{grid}</div>;
};
export default Games;
